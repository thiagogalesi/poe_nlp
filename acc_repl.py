#/usr/bin/env python3
import re
import sys
from collections import Counter


class ONStemer(object):
    decl_endings = ['a', 'i', 'ju',
                    's', 'jar', 'ir',
                    'r', 'is', 'ar',
                    'um', 'jum', 'rr']
    extra_decls = {'loka': 'loki'}
    forbidden_decls = ['tho', 'thorinn']
    def is_decl(self, w1, w2):
        if w1[:3] != w2[:3]:
            return None
        isd = False
        wio = [w1, w2]
        wio.sort()
        wr = wio[0]
        wc = wio[1]
        if wr in self.extra_decls:
            if wc == self.extra_decls[wr]:
                return True
        if any(w in self.forbidden_decls for w in wio):
            return False

        if wr[-1] == 'r':
            wr = wr[:-1]
        for k in sorted(self.decl_endings, key=lambda x:-len(x)):
            if wc == wr + k:
                return True
                #if k == 'rr':
                #    return wc
                #return wr
            if wc == ('%sr%s'%(wr,k)):
                return True
        return None

    def process_decl(self, all_words_s):
        reduced_word = None
        all_words = list(all_words_s)
        all_words.sort()
        awn = []
        rwl = {}
        rwol = [all_words[0]]
        max_rwol_candidates = 6
        awn.append(all_words[0])
        for k in all_words[1:]:
            #if 'baldr' in k:
            #    print(rwol, k)
            #if 'thor' in k:
            #    print(rwol, k)
            reduced_word = None
            for crw in rwol:
                if self.is_decl(crw, k):
                    reduced_word = crw
                    awn.append('%s:%s'%(reduced_word,k))
                    rwl[k] = reduced_word
                    # requeues so it's not dropped
                    if rwol[-1] != crw:
                        rwol.append(crw)
                    break
            if reduced_word:
                continue
            awn.append('%s'%(k))

            if len(rwol) > max_rwol_candidates:
                del rwol[0]
            if len(k) < 4:
                rwol.append(k)
                continue
            rwol.append(k)
        return (awn, rwl)

def on_casefold(txt):
    tr_dict = {'Þ': 'TH', 'á': 'a', 'æ':'ae', 'œ':'oe', 'é':'e',
               'í':'i', 'ð':'th', 'ó':'o', 'ö':'o', 'ú':'u', 'ý':'y', 'þ':'th'}

    txw = re.sub(r'[!,-.:;?"]|[0-9]|\]|\[', '', txt.lower())
    txw = re.sub('\n', ' ', txw)
    trl_text = re.sub('\w', lambda x:tr_dict.get(x.group(), x.group()), txw)
    return trl_text

def main():
    fname = sys.argv[1]
    txtb = open(fname,'rb').read()
    txt = txtb.decode('utf-8')
    trl_text = on_casefold(txt)
    all_words_rep = trl_text.lower().split(' ')
    all_words_cnt = Counter(all_words_rep)
    all_words = set(all_words_rep)
    #open('poewords.txt','w').write('\n'.join(sorted(all_words)))

    rw, rwl = ONStemer().process_decl(all_words)
    open('poewords.txt','w').write('\n'.join(rw))

    r_text = []
    for w in trl_text.split(' '):
        if not w:
            continue
        rword = rwl.get(w, w)
        rword = ONStemer().extra_decls.get(rword, rword)
        r_text.append(rword)

    open('poeutrsl_repl.txt','w').write(' '.join(r_text))
if __name__ == '__main__':
    main()
