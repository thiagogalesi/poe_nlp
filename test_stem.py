from unittest import TestCase
from nose import tools
from acc_repl import ONStemer, on_casefold

class CaseFoldTest(TestCase):
    def test1(self):
        stri = "Bekki breiða, nú skal brúðr með mér"
        stro = "bekki breitha nu skal bruthr meth mer"
        tools.assert_equals(on_casefold(stri), stro)
    def test2(self):
        stri = "hratat um mægi mun hverjum þykkja heima skal-at hvíld nema. Þórr kvað:"
        stro = "hratat um maegi mun hverjum thykkja heima skalat hvild nema thorr kvath"
        tools.assert_equals(on_casefold(stri), stro)
    def test_chars(self):
        stri = "Þáæœéíðóöúýþ"
        stro = "thaaeoeeithoouyth"
        tools.assert_equals(on_casefold(stri), stro)
    def test_symbols(self):
        stri = "-:!;?02345]["
        stro = ""
        tools.assert_equals(on_casefold(stri), stro)


class ONStemerTest(TestCase):
    def testDeclWords(self):
        word_list = [('arma', 'arm'),
                     ('armar', 'arm'),
                     ('arms', 'arm'),
                     ('armr', 'arm'),
                     ('armi', 'arm')] #umlaut
        ons = ONStemer()
        for k in word_list:
            tools.assert_true(ons.is_decl(k[0], k[1]), msg="%s:%s"%(k[0], k[1]))
    def testDeclWords2(self):
        word_list = [('thorr', 'thor'),
                     ('thori', 'thor'),
                     ('thors', 'thor'),
                     ] #umlaut
        ons = ONStemer()
        for k in word_list:
            tools.assert_true(ons.is_decl(k[0], k[1]), msg="%s:%s"%(k[0], k[1]))
    def testDeclWordsExtra(self):
        word_list = [('loki', 'loka')]
        ons = ONStemer()
        for k in word_list:
            tools.assert_true(ons.is_decl(k[0], k[1]), msg="%s:%s"%(k[0], k[1]))

    def testWordSet1(self):
        word_set = set(['ari', 'arm','arma','armar','armbauga','armi'])
        ons = ONStemer()
        ret,_ = ons.process_decl(word_set)
        tools.assert_equals(ret[0], 'ari')
        tools.assert_equals(ret[1], 'arm')
        tools.assert_equals(ret[2], 'arm:arma')
        tools.assert_equals(ret[3], 'arm:armar')
        tools.assert_equals(ret[4], 'armbauga')
        tools.assert_equals(ret[5], 'arm:armi')

    def testWordSet2(self):
        word_set = set(['thor', 'thorf', 'thorfgi', 'thori', 'thoriga', 'thorr', 'thors'])
        ons = ONStemer()
        ret,_ = ons.process_decl(word_set)
        tools.assert_equals(ret[0], 'thor')
        tools.assert_equals(ret[3], 'thor:thori')
        tools.assert_equals(ret[5], 'thor:thorr')
        tools.assert_equals(ret[6], 'thor:thors')

    def testWordSet3(self):
        word_set = set(['baldr', 'baldri', 'baldrs'])
        ons = ONStemer()
        ret,_ = ons.process_decl(word_set)
        tools.assert_equals(ret[0], 'baldr')
        tools.assert_equals(ret[1], 'baldr:baldri')
        tools.assert_equals(ret[2], 'baldr:baldrs')
